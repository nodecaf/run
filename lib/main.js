const path = require('path');
const chokidar = require('chokidar');
let watcher, dieFn, termFn;

/* istanbul ignore next */
function term(){
    this.stop();

    if(watcher){
        watcher.close();
        watcher = null;
    }

    if(!process.env.NODE_ENV)
        setTimeout(() => process.exit(0), 1000);
}

/* istanbul ignore next */
function die(reload, err){
    if(this.log)
        this.log.fatal({ err, type: 'crash' });
    else
        console.error(err);
    if(!reload)
        process.exit(1);
}

function setupHandlers(app, opts){

    if(termFn && dieFn){
        process.off('SIGINT', termFn);
        process.off('SIGTERM', termFn);
        process.off('uncaughtException', dieFn);
        process.off('unhandledRejection', dieFn);
    }

    dieFn = die.bind(app, opts.reload);
    termFn = term.bind(app);

    process.on('SIGINT', termFn);
    process.on('SIGTERM', termFn);
    process.on('uncaughtException', dieFn);
    process.on('unhandledRejection', dieFn);
}

function load(opts){
    let mainPath = module.children[module.children.length -1].id;
    delete require.cache[mainPath];

    try{
        var app = require(opts.path)();
    }
    catch(e){
        console.log(e);
        throw new TypeError('App Path must be a valid nodecaf app path');
    }

    if(app.constructor.name !== 'Nodecaf')
        throw new TypeError('Init function must return a Nodecaf instance');

    let confPath = opts.conf || [];
    let confs = Array.isArray(confPath) ? confPath : [confPath];
    confs.forEach(c => c && app.setup(c));

    setupHandlers(app, opts);

    /* istanbul ignore next */
    if(opts.reload)
        app._fsWatcher = chokidar.watch(opts.path + '/**/*.js', {
            ignored: [ 'node_modules' ],
            persistent: false,
            ignoreInitial: true
        }).on('all', reload.bind(null, app, opts))

    return app;
}

/* istanbul ignore next */
async function reload(app, opts, event, path){
    app._fsWatcher.close();
    await app.stop();

    // Invalidate changed file and all parents up to init function
    var nm = require.cache[path];
    while(nm.parent && nm.parent.exports.name != 'run'){
        nm = require.cache[path].parent;
        delete require.cache[path];
        path = nm.id;
    }

    let newApp = load(opts);

    //newApp.debugEvents = app.debugEvents;
    newApp.log.debug({ class: 'app' }, 'Reloaded');
    await newApp.start();
    //newApp.debugEvents.emit('reloaded', newApp);
}

module.exports = function run(opts = {}){

    // Path for local dev environment WITH BIN.
    let appPath = path.resolve(require.main.path + '/..');

    try{
        appPath = path.resolve(process.cwd(), opts.path);
    }
    catch(e){
        if(opts.path)
            throw new TypeError('App Path must be a valid nodecaf app path');
    }

    opts.path = appPath;
    let app = load(opts);

    /* istanbul ignore next */
    //if(opts.reload)
    //    app.debugEvents = new EventEmitter();

    return app.start();
}
