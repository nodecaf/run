# Nodecaf Run Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.1.2] - 2021-01-25

### Fixed
- bug when running nodecaf v0.11.4 with reload flag

## [v0.1.1] - 2020-08-10

### Added
- support to Nodecaf v0.11.x

### Fixed
- bug trying to reload on changes to `main.js`

## [v0.1.0] - 2020-08-10

### Added
- support to Nodecaf v0.10.x interface

### Removed
- support to Nodecaf apps below v0.10

## [v0.0.4] - 2020-05-19

### Fixed
- reloading of deep child modules that would not take effect unless parents are also reloaded
- app name and version being fetched from wrong package.json when running through bin

## [v0.0.3] - 2020-04-24

### Added
- log dump of erros when loading the app

### Fixed
- bug with reload flag that wouldn't reload app sub-modules

## [v0.0.2] - 2020-04-17

### Fixed
- bug when testing app server instance returned by init function

## [v0.0.1] - 2020-04-16

First officially published version moved from nodecaf-cli

[v0.0.1]: https://gitlab.com/nodecaf/run/-/tags/v0.0.1
[v0.0.2]: https://gitlab.com/nodecaf/run/-/tags/v0.0.2
[v0.0.3]: https://gitlab.com/nodecaf/run/-/tags/v0.0.3
[v0.0.4]: https://gitlab.com/nodecaf/run/-/tags/v0.0.4
[v0.1.0]: https://gitlab.com/nodecaf/run/-/tags/v0.1.0
[v0.1.1]: https://gitlab.com/nodecaf/run/-/tags/v0.1.1
[v0.1.2]: https://gitlab.com/nodecaf/run/-/tags/v0.1.2
