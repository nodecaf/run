const assert = require('assert');

process.env.NODE_ENV = 'testing';

// Address for the tests' local servers to listen.
const LOCAL_HOST = 'http://localhost:80'

const { context } = require('muhb');
let base = context(LOCAL_HOST);

describe('run()', () => {
    const run = require('../lib/main');
    const path = require('path');

    let appPath = path.resolve('./test/res');
    let bAppPath = path.resolve('./test/res/bad-index');

    it('Should fail with bad init path', () => {
        assert.throws(() => run());
        assert.throws(() => run({ path: true }));
    });

    it('Should fail when init returns other than App', () => {
        assert.throws(() => run({ path: bAppPath }), /Nodecaf/);
    });

    it('Should load the proper app name', async () => {
        const Tempper = require('tempper');
        let tmp = new Tempper();
        tmp.addFile('./test/res/package.json', './package.json');
        tmp.mkdir('./lib');
        tmp.addFile('./test/res/reload.js', './lib/main.js');
        let app = await run({ path: '.' });
        assert.strictEqual(app._name, 'test-run');
        await app.stop();
        tmp.clear();
    });

    it('Should run the given app server', async () => {
        let app = await run({ path: appPath });
        let { body } = await base.get('bar');
        assert.strictEqual(body, 'foo');
        await app.stop();
    });

    it('Should inject the given conf file', async () => {
        let app = await run({ path: appPath, conf: 'test/res/conf.toml' });
        let { body } = await base.get('bar');
        assert.strictEqual(body, 'value');
        await app.stop();
    });

    it('Should inject multiple conf files', async () => {
        let app = await run({ path: appPath, conf: [ 'test/res/conf.toml', './package.json' ] });
        let { body } = await base.get('bar');
        assert.strictEqual(body, 'nodecaf-run');
        await app.stop();
    });

    // TODO fix cryptic error EPERM on writing and closing the watched file
    /*
    describe.only('Reloading', () => {
        const fs = require('fs');


        beforeEach(() => tmp.refresh());

        it('Shoud ...', (done) => {
            tmp.addFile('./test/res/package.json', './package.json');
            tmp.mkdir('./lib');
            tmp.addFile('./test/res/reload.js', './lib/main.js');

            (async function(){
                let app = await run({ reload: true, path: '.' });

                let { body } = await base.get('foo');
                assert.strictEqual(body, 'A');

                let data = fs.readFileSync('./lib/main.js', 'utf-8');
                let newData = [ 'const test = \'B\';', ...data.split(/[\r\n]+/).slice(1) ].join('\r\n');

                app.debugEvents.on('reloaded', async newApp => {

                    console.log('GOT HERE');

                    app.fsWatcher.close();
                    await newApp.stop();
                    done();
                });

                fs.writeFile('./lib/main.js', newData, console.log);
            })();
        });
    });*/
});

// describe('Watch Conf Files', () => {
//     const AppServer = require('../lib/app-server');
//
//     it('Should watch changes on layered config files [this.watchConfFiles]', async function(){
//         const fs = require('fs');
//         fs.copyFileSync('./test/res/conf.toml', './node_modules/conf.toml');
//         let app = new AppServer();
//         app.watchConfFiles = true;
//         app.setup('./node_modules/conf.toml');
//         await app.start();
//         await new Promise( done => {
//             app.confort.on('reload', () => setTimeout(done, 1000));
//             fs.writeFileSync('./node_modules/conf.toml', 'key = \'value2\'');
//         });
//         await app.stop();
//         assert.strictEqual(app.conf.key, 'value2');
//     });
//
// });
