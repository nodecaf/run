
const Nodecaf = require('nodecaf');

module.exports = () => new Nodecaf({
    conf: { port: 80 },
    api({ get }){
        get('/bar', ({ res, conf }) => {
            res.type('text');
            res.end(conf.name || conf.key || 'foo');
        });
    }
});
