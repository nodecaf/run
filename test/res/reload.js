const test = 'A';
const Nodecaf = require(module.parent.paths[1] + '/nodecaf');

module.exports = () => new Nodecaf({
    api({ get }){
        get('/foo', ({ res }) => {
            res.set('Content-Type', 'text/plain');
            res.end(test);
        });
    }
});
